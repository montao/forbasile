# Go Plugin

The code in this repository uses the `plugin` package in Go 1.10 (see https://tip.golang.org/pkg/plugin/). 

## Requirements
The plugin system requires Go version 1.8.  At this time, it only supports plugins on Linux (not tested with other operating systems). 

The body (that is, some Go code) of a function should be a
program argument. Maybe


    go run forbasile.go SUM 'return x+y' 3 5


where the above is expected to print 8 with some interesting information about it, and of course


    go run forbasile.go SUMSQUARE 'return x*x + y*y' 3 4


is expected to print 25
and the body could even be some more complex Go statement.

## A Pluggable System
The code in this repository implements a simple plugin generator.  


```
> go run forbasile.go sum
.. calling it with xx=3 yy=5
start of SUM: x=3, y=5
result of SUM (3, 5) is 8
```

